%% D_ICA_labeling_resting_20141217

% The central idea here is to take the full data and label the ICA based on
% the data across runs. The labeled components will then be allocated to
% the individual runs, such that ICA exclusion can be performed on the run
% data.

% required functions:
% - cm_label_ica_gui_20180116

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.eeg_root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/';
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
% add ConMemEEG tools
pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];            addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];            addpath(genpath(pn.THGtools));
pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];         addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'stroop';

%% SELECT ID for visual screening

ID = 2214;

%% load data & start GUI

% load data (full data)
load([pn.EEG, num2str(ID), '_', condEEG, '_EEG_Rlm_Fhl_Ica.mat'],'data')

if ~isfield(data.iclabels,'emg')
    data.iclabels.emg = [];
end

% load config (ICA version)
load([pn.History, num2str(ID), '_', condEEG, '_config.mat'],'config')

% settings for ICA

% ica config
cfg.method           = 'runica';
%cfg.channel          = {'all','-ECG','-A2'};
cfg.trials           = 'all';
cfg.numcomponent     = 'all';
cfg.demean           = 'no';
cfg.runica.extended  = 1;

% ICA solution for segments
cfg.unmixing     = data.unmixing;
cfg.topolabel    = data.topolabel;

% ICA (from weights)

% components
comp = ft_componentanalysis(cfg,data);
    
% include electrode information
comp.elec = data.elec;

% include ICA labeling
comp.iclabels = data.iclabels;

% clear cfg
clear cfg

% label ICs

% settings
cfg.topoall  = 'yes';
cfg.chanlocs = data.chanlocs;

% label electrodes
data.iclabels = cm_label_ica_gui_20180116(cfg,comp);

% keep labels in config
config.ica1.iclabels.manual = data.iclabels;

% clear variables
clear cfg

% save data

% save data (full data)
save([pn.EEG, num2str(ID), '_', condEEG, '_EEG_Rlm_Fhl_Ica.mat'],'data')

% save config (ICA version)
save([pn.History, num2str(ID), '_', condEEG, '_config.mat'],'config')

% clear variables

clear BLOCK ID comp config data
