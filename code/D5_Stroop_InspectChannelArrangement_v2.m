% D5_InspectChannelArrangement
% Inspect the correlation structure of the channel arrangement for
% potential misfits.

% 180126 | adapted for STSWD Stroop

pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/'];
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/'];
pn.figures      = [pn.eeg_root, 'C_figures/C_ChannelCheck/']; mkdir(pn.figures);

%% define IDs for visual screening
% N = 47; without 1213; N = 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';...
    '1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';...
    '1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';...
    '1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

for id = 1:numel(IDs)
    disp(num2str(id))
    data_EEG = [];
    load([pn.EEG, num2str(IDs{id}), '_stroop1_EEG_Raw_Rlm_Flh_Res.mat'],'data_EEG')
    tmp = corrcoef(data_EEG.trial{1}');
    Rmat(id,1:66,1:66) = tmp(1:66,1:66);
end

h = figure;
for indID = 1:size(Rmat,1)
    subplot(10,10,indID);
    imagesc(squeeze(Rmat(indID,:,:)));
    title(IDs{indID});
end
pn.plotFolder = pn.figures;
figureName = 'ChannelCheck_correlation';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
