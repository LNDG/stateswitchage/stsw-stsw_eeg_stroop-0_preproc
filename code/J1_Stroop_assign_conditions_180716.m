%% I_STSW_prep_data_for_analysis_180514

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.eeg_root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/';
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
pn.helper       = [pn.eeg_root, 'A_scripts/helper/']; addpath(genpath(pn.helper));
pn.logs         = [pn.eeg_root, 'Y_logs/H_ArtifactCorrection/']; mkdir(pn.logs);
% add ConMemEEG tools
pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];            addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];            addpath(genpath(pn.THGtools));
pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];         addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% define IDs for segmentation

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%%  loop IDs

setting.plot = 1; % plot imagesc of resulting timeseries

% prior to loop create matrixes for overviews over artifact-free trials and total trial numbers
overview_nartfree_trials = {};
overview_trial_numbers = {};
ID_unavailable = cell(length(IDs),1);
for id = 1:length(IDs)
    display(['processing ID ' num2str(IDs{id})]);
    for iRun = 1:2
        try
        
            if strcmp(IDs{id}, '2160') & iRun==2
                continue;
            end
            
            condEEG = ['stroop', num2str(iRun)];
            
            % load data
            load([pn.EEG, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg'], 'data');
            dataICA = load([pn.EEG, IDs{id}, '_stroop_EEG_Rlm_Fhl_Ica'],'data');
            data.elec = dataICA.data.elec;
            data.chanlocs = dataICA.data.chanlocs; clear dataICA;
            % load config
            load([pn.History, IDs{id}, '_', condEEG, '_config'],'config')

        %%  define parameter

            % ICs to reject
            iclabels    = config.ica1.iclabels.manual;
            ics2reject = [iclabels.bli(:); iclabels.mov(:); iclabels.hrt(:); iclabels.ref(:)];
            clear iclabels

            % trials to remove
            trls2remove = config.ArtDect.trials;

            % channels to interpolate
            chns2interp = config.ArtDect.channels;

        %% ICA (from weights)

            % ica config
            cfg.method           = 'runica';
            cfg.channel          = {'all','-ECG','-A2'};
            cfg.trials           = 'all';
            cfg.numcomponent     = 'all';
            cfg.demean           = 'yes';
            cfg.runica.extended  = 1;

            % ICA solution
            cfg.unmixing     = config.ica1.unmixing;
            cfg.topolabel    = config.ica1.topolabel;

            % components
            comp = ft_componentanalysis(cfg,data);

            % clear cfg
            clear cfg data

        %% remove components

            % cfg for rejecting components (reject: blinks, eye movements, ecg, ref)
            cfg.component = sortrows(ics2reject)';
            cfg.demean    = 'yes';

            % reject components
            data = ft_rejectcomponent(cfg,comp);

            % clear cfg
            clear cfg comp ics2reject

        %%  remove trials and create proper indexing

            % JQK 14.05.: config.trl also has to be shifted accordingly!!!
        
            % IMPORTANT: 
            % There are some subjects, for whom some trials are missing in
            % the beginning. For these subjects, we 'remove' the originally
            % missing trials from the indexing as well. Hence, we treat
            % them as if they were excluded due to being artefactual.
        
            trialsMissingAtOnset = 81-size(data.trial,2);
            
            % Remove the first trial that IS present in the data as it may
            % not be full length.
            if trialsMissingAtOnset > 0
                trls2remove=[trls2remove;1];
            end
            
            % define trials to keep
            trials               = 1:length(data.trial);
            trials(trls2remove)  = [];

            if trialsMissingAtOnset > 0
                trials=trials+trialsMissingAtOnset;
                data.time = cat(2,repmat(data.time(end),1,trialsMissingAtOnset),data.time);
                data.trial = cat(2,repmat(data.trial(end),1,trialsMissingAtOnset),data.trial);
                config.trl = [NaN(trialsMissingAtOnset,3); config.trl];
            end

            % config for deleting trials
            cfg.trials   = trials;

            % remove trials
            data   = ft_preprocessing(cfg,data);
            nartfree_trials = trials;
            overview_nartfree_trials{id,iRun} = nartfree_trials;

            config.nartfree_trials = nartfree_trials;
            
            % clear variables
            clear cfg trials trls2remove

        %% remove eye & reference channels (before interpolation)

            cfg.channel     = {'all','-IOR','-LHEOG','-RHEOG','-A1'};
            cfg.demean      = 'yes';

            % remove channels
            tmpdat = ft_preprocessing(cfg,data);

            % clear cfg
            clear cfg

            % interpolation cfg
            cfg.method     = 'spline';
            cfg.badchannel = tmpdat.label(chns2interp);
            cfg.trials     = 'all';
            cfg.lambda     = 1e-5; 
            cfg.order      = 4; 
            cfg.elec       = config.elec;

            % interpolate
            tmpdat = ft_channelrepair(cfg,tmpdat);

            % clear cfg
            clear cfg chns2interp

        %%  replace interpolated data

            for j = 1:length(data.trial)

                data.trial{j}(1:60,:) = tmpdat.trial{j};

            end; clear j

            % clear temporary data
            clear tmpdat

        %%  keep channel x trial artifacts  

            data.ArtDect.channels_x_trials = config.ArtDect.channels_x_trials;            

        %%  save data
            save([pn.EEG, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art'],'data')

        %% create overview of percentage of excluded trials, update config.trl

            tmp_trialNumbers(1,1) = length(data.trial);
            tmp_trialNumbers(2,1) = 81; % maximum amount of trials
            tmp_trialNumbers(3,1) = (100-((tmp_trialNumbers(1,1)/tmp_trialNumbers(2,1))*100)); % percentage of excluded trials
            overview_trial_numbers{id, iRun} = tmp_trialNumbers;
            config.overview_trial_numbers = tmp_trialNumbers;
            clear tmp_trialNumbers;
            
        %%  update config.trl with trigger codes

            % add info whether trial is included in final set; 1 = trial containing no artifact
            config.trl(:,4)                 = 0;
            config.trl(nartfree_trials, 4)  = 1;
            % add trial number
            config.trl(:,5) = 0;
            config.trl(nartfree_trials, 5)  = nartfree_trials;
            % load marker file
            mrk = config.mrk;
            for i = 1:size(mrk,2)
               mrk_val{i,1} = mrk(1,i).value;
            end
            % add onset and offset triggers
            config.trl(:,6) = 16;
            config.trl(:,7) = 64;
            % save config
            save([pn.History, IDs{id}, '_', condEEG, '_config'],'config')
            
        catch ME
            warning('Error occured. Please check.');
            rethrow(ME)
            fprintf('\n ID not availble! Skip! \n')
            ID_unavailable{id,iRun} = (IDs{id});
        end % try...catch
    end % run
end % id