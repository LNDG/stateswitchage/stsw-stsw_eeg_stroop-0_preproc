%% E_segmentation_for_artifact_correction

% 170921 | JQK adapted from MD script
% 180420 | adapted for stroop data
% 180730 | For 2112, 2121 and 2214, perform additional channel
%           interpolation as has been done prior to ICA.

% Note that a slight jitter may have occured between the end 
% of the trial and retrieving the audio, essentially creating 
% an interstimulus interval. Therefore we segment the end 
% to the expected length of 3s +-1.5s.

% Dropping ECG here.

% If requested trial onset is smaller than 0, onset will be set to 1,
% resulting in different trial length. These trials should later be
% excluded. [Probelm for 1257 Stroop 2, 1151 S2, likely 1120 S1, 1173 S1, 1219 S2].

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.eeg_root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/';
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
pn.helper       = [pn.eeg_root, 'A_scripts/helper/']; addpath(genpath(pn.helper));
pn.timingCheck  = [pn.eeg_root, 'C_figures/D_timingCheck/']; mkdir(pn.timingCheck);
% add ConMemEEG tools
pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];            addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];            addpath(genpath(pn.THGtools));
pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];         addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% define IDs for segmentation

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%  loop IDs
clc;
for id = 1:length(IDs)
    display(['processing ID ' num2str(IDs{id})]);
    for iRun = 1:2
        %%  load raw data

        condEEG = ['stroop', num2str(iRun)];

        if ~exist([pn.EEG, IDs{id},'_', condEEG, '_EEG_Rlm_Fhl_rdSeg.mat'],'file')
        try  

        % load config
        load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');

        % copy ICA labeling info
        configWithICA = load([pn.History, IDs{id}, '_stroop_config.mat'],'config');

        config.ica1 = configWithICA.config.ica1;

        %% ---- generate segmentation ---- %%

        % load marker file
        mrk = config.mrk;

        for i = 1:size(mrk,2)
            mrk_val{i,1} = mrk(1,i).value;
        end

        % generate trial structure
        indOnset = find(strcmp(mrk_val(:,:),'S 16')); % (fix cue onset trigger = 'S 16')
        indOnset = sortrows(indOnset,1);
        indOffset = find(strcmp(mrk_val(:,:),'S 64')); % (audio encoding of previous trial = 'S 64')
        indOffset = sortrows(indOffset,1);

        h = figure;
        plot(diff([mrk(indOnset).sample]))
        hold on; plot(diff([mrk(indOffset).sample]))
        title([IDs{id}, ' ', condEEG, ' Timing']);
        pn.plotFolder = pn.timingCheck;
        figureName = ['TimingCheck_', IDs{id}, '_', condEEG];
        saveas(h, [pn.plotFolder, figureName], 'fig');
        saveas(h, [pn.plotFolder, figureName], 'epsc');
        saveas(h, [pn.plotFolder, figureName], 'png');
        close(h);

        % Stim trials
        trl = zeros(length(indOnset),3);
        TOI1 = 1500; % segmentation before trigger
        TOI2 = 1500; % segmentation following trigger

        % Note that a slight jitter may have occured between the end 
        % of the trial and retrieving the audio, essentially creating 
        % an interstimulus interval. Therefore we resegment the end 
        % to the expected length of 3s +-1.5s.

        for j = 1:size(trl,1)
            trl(j,1) = mrk(1,indOnset(j)).sample - TOI1;        % segmentation from 1500 ms before stim
            trl(j,2) = mrk(1,indOnset(j)).sample +3000 + TOI2;  % to 1500 ms after response interval
            check_trl(j,1) = mrk(1,indOffset(j)).sample + TOI2;
            trl(j,3) = -TOI1;                                   % offset
            % if onset is not included: reduce trial length
            if trl(j,1) < 1
                trl(j,3) = (trl(j,3)+1-trl(j,1));
                trl(j,1) = trl(j,1)+1-trl(j,1);
            end
        end; clear j

        % figure; plot(trl(:,2)-trl(:,1))

        % check that original triggering falls into expected range!
        if check_trl(:,1)-trl(:,1) > 7000 | check_trl(:,1)-trl(:,1) < 6000
            warning('Original triggering may be wrong. Please check!');
        end

        % add trial structure to config
        config.trl = trl;

        % save config
        save([pn.History, IDs{id},'_', condEEG, '_config.mat'],'config')

        %% ----  load, filter, and re-reference raw data ---- %%

        % define reading & preprocessing parameters
        % read in all data first

        config.data_file = [pn.eeg_root, 'B_data/B_EEG_ByPrePost/',IDs{id},'_',condEEG,'_EEG.set'];

        cfg             = [];
        cfg.datafile    = [config.data_file];                
        cfg.trl         = config.trl;

        cfg.channel     = {'all'};
        cfg.implicitref = 'REF'; % recover implicit reference

        data            = ft_preprocessing(cfg);

        %% SWITCH CHANNELS ACCORDING TO ARRANGEMENT!

        if max(strcmp(IDs{id}, {'1126'; '2227'}))==1
            data = SS_switchChannels_GreenYellow(data);       % green and yellow boxes exchanged
            data = SS_switchChannels_Study_noA1(data);        % TP9 and TP10 exchanged manually
        elseif max(strcmp(IDs{id}, {'1216'}))==1
            data = SS_switchChannels_GreenYellow(data);       % green and yellow boxes exchanged
            data = SS_switchChannels_Study(data);             % TP9(A1) and TP10(FCz) wrongly ordered in workspace
        elseif max(strcmp(IDs{id}, {'1118'; '1215'; '1124'}))==1
            data = SS_switchChannels_Study_noA1(data);        % TP9 and TP10 exchanged manually
        else 
            data = SS_switchChannels_Study(data);             % TP9(A1) and TP10(FCz) wrongly ordered in workspace
        end
        data = SS_changeChannels(data);

        %% filter & reref EEG data
        cfg             = [];

        cfg.channel     = {'all', '-ECG'};

        cfg.continuous  = 'yes';
        cfg.demean      = 'yes';

        cfg.reref       = 'yes';
        cfg.refchannel  = {'A1','A2'};
        cfg.implicitref = 'A2';

        cfg.hpfilter    = 'yes';
        cfg.hpfreq      = .2;
        cfg.hpfiltord   = 4;
        cfg.hpfilttype  = 'but';

        cfg.lpfilter    = 'yes';
        cfg.lpfreq      = 125;
        cfg.lpfiltord   = 4;
        cfg.lpfilttype  = 'but';

        % get data
        data = ft_preprocessing(cfg, data);
        flt = cfg;

        % clear cfg structure
        clear cfg

        %% ----  resampling [500 Hz] ---- %%

        % define settings for resampling
        cfg.resamplefs = 500;                           % raw_eeg (ICA loop) at srate of 250Hz! now resample raw files (original files) to 500 Hz!
        cfg.detrend    = 'no';
        cfg.feedback   = 'no';
        cfg.trials     = 'all';

        % resample ALL data
        data = ft_resampledata(cfg,data);

        resample = cfg;
        % clear variables
        clear cfg

        % update config
        config.seg.flt = flt;
        config.seg.resample = resample;
                
        %% for select IDs, interpolate channels prior to ICA
                     
        if strcmp(IDs{id}, '2112') || strcmp(IDs{id}, '2121') || strcmp(IDs{id}, '2214')
            cfg = [];
            cfg.channel = {'all','-ECG','-A2'};
            dataSelect = ft_preprocessing(cfg,data);
            % fieldtrip format electrode information
            load([pn.THGtools, 'electrodelayouts/realistic_1005.mat'])
            data.elec = cm_elec2dataelec_20170919(realistic_1005,dataSelect);
            cfg = [];
            cfg.method     = 'spline';
            if strcmp(IDs{id}, '2112')
                cfg.badchannel = {'P7'; 'FCz'};
            elseif strcmp(IDs{id}, '2121')
                cfg.badchannel = {'PO10'};
            elseif strcmp(IDs{id}, '2214')
                cfg.badchannel = {'POz'};
            end
            cfg.trials     = 'all';
            cfg.lambda     = 1e-5; 
            cfg.order      = 4; 
            cfg.elec       = data.elec;
            data = ft_channelrepair(cfg,data);
            config.interpPreICA = cfg; 
            clear cfg;
        end
        
        %% save data

        % save config, data
        save([pn.EEG, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg'],'data')
        save([pn.History, IDs{id}, '_', condEEG, '_config'],'config')

        % clear variables
        clear config data data_eye data_EEG indOnset indOffset mrk mrk_val trl TOI1 TOI2

        catch
            warning(['Error for ', IDs{id}, ' ', condEEG]);
            continue
        end
        else
            disp([IDs{id}, ' ', condEEG, ' appears to have already been processed.'])
        end % exist
    end; clear iRun
end; clear id
