%% A_prepare_preprocessing_StateSwitch_170913

% prepare (i.e. filter + downsample data for ICA1)

% INPUT: eeglab structures: EEG data, merged with eye tracking data

% 170913 | JQK adapted function from MD's LC-NE scripts
% 180108 | adapted to stroop
% 180126 | switch channels according to task data
% 180223 | added OAs

% 2160 stroop 2 missing

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/'];
pn.stroop1_In   = [pn.eeg_root, 'B_data/B_EEG_ByPrePost/'];
pn.stroop2_In   = [pn.eeg_root, 'B_data/B_EEG_ByPrePost/'];
pn.eyePlots     = [pn.eeg_root, 'C_figures/B_EyeChannels/']; mkdir(pn.eyePlots);
pn.EEG_out      = [pn.eeg_root, 'B_data/C_EEG_FT/']; mkdir(pn.EEG_out);
pn.History_out  = [pn.eeg_root, 'B_data/D_History/']; mkdir(pn.History_out);
pn.scripts      = [pn.eeg_root, 'A_scripts']; addpath(genpath(pn.scripts));

%% add external tools

pn.toolRoot     = pn.eeg_root;
pn.MWBtools     = [pn.toolRoot, 'T_Tools/fnct_MWB'];            addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.toolRoot, 'T_Tools/fnct_THG'];            addpath(genpath(pn.THGtools));
pn.commontools  = [pn.toolRoot, 'T_Tools/fnct_common'];         addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.toolRoot, 'T_Tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.toolRoot, 'T_Tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% define IDs for preprocessing

% N = 48 YAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs (excluding pilots);
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% loop IDs
for id = 1:length(IDs)

%%  preprocessing
    
    for indRun = 1:2
    
        display(['processing ID ' IDs{id}, ' Run ', num2str(indRun)]);
        
        condEEG = ['stroop', num2str(indRun)];
        
        file = [pn.([condEEG, '_In']), IDs{id} , '_', condEEG, '_EEG.set'];

    %%  load data

        cfg_preprocessing           = [];
        cfg_preprocessing.datafile  = file;

        % load header & event information
        config              = ft_read_header(cfg_preprocessing.datafile);
        config.data_file    = cfg_preprocessing.datafile;
        config.mrk          = ft_read_event(cfg_preprocessing.datafile);

        % define reading & preprocessing parameters
        cfg_preprocessing.channel     = {'all'};
        cfg_preprocessing.implicitref = 'REF'; % recover implicit reference
        
        % get all data first, then apply specific steps only for subsets
        data_eyeEEG = ft_preprocessing(cfg_preprocessing);

        %% SWITCH CHANNELS ACCORDING TO ARRANGEMENT!
        
        if max(strcmp(IDs{id}, {'1126'; '2227'}))==1
            data_eyeEEG = SS_switchChannels_GreenYellow(data_eyeEEG);       % green and yellow boxes exchanged
            data_eyeEEG = SS_switchChannels_Study_noA1(data_eyeEEG);        % TP9 and TP10 exchanged manually
        elseif max(strcmp(IDs{id}, {'1216'}))==1
            data_eyeEEG = SS_switchChannels_GreenYellow(data_eyeEEG);       % green and yellow boxes exchanged
            data_eyeEEG = SS_switchChannels_Study(data_eyeEEG);             % TP9(A1) and TP10(FCz) wrongly ordered in workspace
        elseif max(strcmp(IDs{id}, {'1118'; '1215'; '1124'}))==1
            data_eyeEEG = SS_switchChannels_Study_noA1(data_eyeEEG);        % TP9 and TP10 exchanged manually
        else 
            data_eyeEEG = SS_switchChannels_Study(data_eyeEEG);             % TP9(A1) and TP10(FCz) wrongly ordered in workspace
        end
        data_eyeEEG = SS_changeChannels(data_eyeEEG);
        
        %% Plot excerpt from eye channels
        
        h = figure;
        subplot(3,1,1);
        plot(zscore(data_eyeEEG.trial{1,1}(63,1:5000),[],2)); % LHEOG
        hold on; plot(zscore(data_eyeEEG.trial{1,1}(64,1:5000),[],2)); % RHEOG
        xlim([0 5000]); legend({'LHEOG'; 'RHEOG'});
        subplot(3,1,2);
        %hold on; plot(zscore(data_eyeEEG.trial{1,1}(65,1:5000),[],2)); % ECG
        hold on; plot(zscore(data_eyeEEG.trial{1,1}(62,1:5000),[],2)); % IOR
        hold on; plot(zscore(data_eyeEEG.trial{1,1}(24,1:5000),[],2)); % FT8
        xlim([0 5000]); legend({'IOR'; 'FT8'});
        subplot(3,1,3);
        if size(data_eyeEEG.trial{1,1},1)>66
            hold on; plot(zscore(data_eyeEEG.trial{1,1}(67,1:5000),[],2)); % left Gaze X
            hold on; plot(zscore(data_eyeEEG.trial{1,1}(68,1:5000),[],2)); % left Gaze Y
            xlim([0 5000]);legend({'left X'; 'left Y'});
        end
        pn.plotFolder = pn.eyePlots;
        figureName = [IDs{id} ,'_r',num2str(indRun), '_', condEEG, '_eyeChannels'];
        saveas(h, [pn.plotFolder, figureName], 'png');
        close(h);

        %% continue preprocessing
        
        % spare ECG from reref!
        tmp.data_ECG = data_eyeEEG;

        cfg_ECG                 = [];
        cfg_ECG.channel         = {'ECG'};
        cfg_ECG.bsfilter        = 'yes';
        cfg_ECG.bsfiltord       = 4;
        cfg_ECG.bsfreq          = [48 52];
        tmp.data_ECG            = ft_preprocessing(cfg_ECG, tmp.data_ECG);

        % exclude ET, ECG channel (and others) here?

        cfg_preprocessing               = [];                                 
        cfg_preprocessing.channel       = {'all'};

        cfg_preprocessing.continuous  = 'yes';
        cfg_preprocessing.demean      = 'yes';

        cfg_preprocessing.reref       = 'yes';
        cfg_preprocessing.refchannel  = {'A1', 'A2'};
        cfg_preprocessing.implicitref = 'A2';

        cfg_preprocessing.hpfilter    = 'yes';
        cfg_preprocessing.hpfreq      = 1;
        cfg_preprocessing.hpfiltord   = 4;
        cfg_preprocessing.hpfilttype  = 'but';

        cfg_preprocessing.lpfilter    = 'yes';
        cfg_preprocessing.lpfreq      = 100;
        cfg_preprocessing.lpfiltord   = 4;
        cfg_preprocessing.lpfilttype  = 'but';

    %     cfg_preprocessing.bsfilter        = 'yes';
    %     cfg_preprocessing.bsfiltord       = 4;
    %     cfg_preprocessing.bsfreq          = [48 52];

        % define settings for resampling
        cfg_resample.resamplefs = 250;
        cfg_resample.detrend    = 'no';
        cfg_resample.feedback   = 'no';
        cfg_resample.trials     = 'all';

        % get data
        % load complete data at once & resample afterwards
        data_EEG = ft_preprocessing(cfg_preprocessing, data_eyeEEG);

        % after reref, copy original ECG (we don't want it rerefed)
        data_EEG.trial{1,1}(find(strcmp(data_EEG.label , 'ECG')),:) = tmp.data_ECG.trial{1,1};
        clear tmp;

        data_EEG = ft_resampledata(cfg_resample,data_EEG);
        % change data precision to single
        for t = 1:length(data_EEG.trial)
            data_EEG.trial{t} = single(data_EEG.trial{t});
        end; clear t

    %%  save data

        save([pn.EEG_out, IDs{id}, '_', condEEG, '_EEG_Raw_Rlm_Flh_Res.mat'],'data_EEG', '-v7.3');
        save([pn.EEG_out, IDs{id}, '_', condEEG, '_EEG_Raw.mat'],'data_eyeEEG', '-v7.3');
        % save config
        save([pn.History_out, IDs{id}, '_', condEEG, '_config.mat'],'config');
        % clear variables
        clear cfg_* config data_EEG data_eyeEEG file
    
    end % run loop

end; clear id;
