%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.study            = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root         = [pn.study, 'dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/'];
pn.stroopEEGlab     = [pn.eeg_root, 'B_data/B_EEG_ByPrePost/'];
pn.stroopFieldTrip  = [pn.eeg_root, 'B_data/C_EEG_FT/'];

%% define IDs for preprocessing

% N = 48 YAs; N = 53 OAs (excluding pilots);
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';...
    '1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';...
    '1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';...
    '1266';'1268';'1270';'1276';'1281';'2104';'2107';'2108';'2112';'2118';'2120';...
    '2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';...
    '2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';...
    '2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';...
    '2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% check which files are available

for indID = 1:numel(IDs)
   for indStroop = 1:2
      files = dir([pn.stroopEEGlab, IDs{indID}, '_stroop',num2str(indStroop),'*']);
      if min(size(files))>0
          dataAvailable.EEGlab(indID,indStroop) = 1;
      else
          dataAvailable.EEGlab(indID,indStroop) = 0;
      end
      
      files = dir([pn.stroopFieldTrip, IDs{indID}, '_stroop',num2str(indStroop),'*']);
      if min(size(files))>0
          dataAvailable.FieldTrip(indID,indStroop) = 1;
      else
          dataAvailable.FieldTrip(indID,indStroop) = 0;
      end
   end
end

figure; 
subplot(1,2,1); imagesc(dataAvailable.EEGlab);
subplot(1,2,2); imagesc(dataAvailable.FieldTrip);