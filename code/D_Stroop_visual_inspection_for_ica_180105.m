%% D_STSW_visual_inspection_for_ica_180105

% 170913 | JQK adapted from MD script
% 180105 | JQK adapted for STSWD study

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/'];
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/']; mkdir(pn.EEG);
pn.History      = [pn.eeg_root, 'B_data/D_History/']; mkdir(pn.History);

pn.MWBtools     = [pn.eeg_root, 'T_Tools/fnct_MWB'];            addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.eeg_root, 'T_Tools/fnct_THG'];            addpath(genpath(pn.THGtools));
pn.commontools  = [pn.eeg_root, 'T_Tools/fnct_common'];         addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.eeg_root, 'T_Tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.eeg_root, 'T_Tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% define IDs for visual screening

% N = 48 YAs;
%IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs (excluding pilots);
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% loop IDs

for id = 1:length(IDs)
    for indRun = 1:2
        
        keep pn IDs id indRun;
        
        %% load data, start screening
        
        condEEG = ['stroop', num2str(indRun)];
        
        ID = str2num(IDs{id});
        disp(['Processing ', num2str(ID), ' ', condEEG]);
        % load config
        load([pn.History, num2str(ID), '_', condEEG, '_config.mat'],'config')
        % load data
        load([pn.EEG, num2str(ID), '_', condEEG, '_EEG_Raw_Rlm_Flh_Res.mat'],'data_EEG')
        % data browser
        if ~isfield(config,'visual_inspection')
            cfg = [];
            cfg.continuous      = 'yes';                           
            cfg.preproc.demean  = 'yes';  % Baselinekorrektur
            cfg.blocksize       = 25;
            cfg.ylim            = [-50 50];
            cfg.viewmode        = 'vertical';
            cfg.plotlabels      = 'yes';
            % cfg.channelcolormap = [];
        else
            cfg = config.visual_inspection;
        end

        cfg.channel = {'all','-ECG', '-A1', '-REF'};

        % inspect data
        cfg = ft_databrowser(cfg,data_EEG);

        % check if ICA conducted on this dataset; if so new artifacts not saved
        if ~isfield(config,'ica1')

            % marked segments
            reject = cfg.artfctdef.visual.artifact;

            % generate "trial" (i.e. segment) structure for ICA
            if exist('reject','var')
                if min(size(reject)) ~= 0 % JQK fix: 170915
                    for j = 1:size(reject,1)+1
                        if j == 1
                            trl(j,:) = [1                reject(j,1)-1  0];
                        elseif j <= size(reject,1)
                            trl(j,:) = [reject(j-1,2)+1  reject(j,1)-1  0];
                        else
                            trl(j,:) = [reject(j-1,2)+1  size(data_EEG.trial{1},2) 0];
                        end
                    end; clear j
                else
                    trl = [1 size(data_EEG.trial{1},2) 0];
                end
            end

            % eliminate trials without data points
            ex = find((trl(:,1)-trl(:,2))==0);
            trl(ex,:) = []; clear ex

            % eliminate trials shorter than 3 sec (! srate = 250 Hz)
            ex = find((trl(:,2)-trl(:,1))<750);
            trl(ex,:) = []; clear ex

            % add to config structure
            config.visual_inspection = cfg;
            config.trl_ica1          = trl;

            % save config
            save([pn.History, num2str(ID), '_', condEEG, '_config.mat'],'config')

        else

            warning('ICA already conducted. Changes not saved.')

        end
    end % run loop
end % id loop
