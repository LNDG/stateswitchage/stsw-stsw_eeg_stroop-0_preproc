[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## EEG Stroop preprocessing

**Maintainer:** [Julian Q. Kosciessa](kosciessa@mpib-berlin.mpg.de)

Most of the steps will be conducted using FieldTrip. To keep the script structure identical, eeglab will be used for a first read-in of the data, although this step may be skipped in the future. eeglab is used for the task data, as the eye tracking data can be read in and matched to the EEG using the eyelab toolbox.

**B_Stroop_importEEG_180424**

- Split EEG data into runs (eeglab toolbox)

**C_Stroop_prepare_preprocessing_180118**

- Prepare for ICA
- Read into FieldTrip format
- Switch channels
- Reref (REF(A1)+A2, A2 as implicit ref), 250 Hz downsampling, 4th order Butterworth 1-100 Hz BPF
- (48-52 Hz bandstop filter – not applied for now)
- no reref for ECG

**D_Stroop_visual_inspection_for_ica_180105**

- Visual inspection for ICA

**D5_Stroop_InspectChannelArrangement_v2**

- This step serves to check the adjeacnecy of channels, just in case there were any problems with switched channel indices in the raw data.

**E_Stroop_ica1_180126**

- Conduct first ICA1, run locally due to smaller amount of data

**F_Stroop_ICA_labeling_180127**

- Manual labeling of artefactual ICA components

**G_Stroop_segmentation_raw_data_180420**

- Segmentation

**H_Stroop_automatic_artifact_correction_180423**

- Automatic artifact correction, interpolation

**I_STSW_prep_data_for_analysis_180514**

- Apply correction procedures (e.g. remove trials); correct trial assignment for those with missing recording onsets

**J1_Stroop_assignCoherence_180716**

- assign congruent/incongruent conditions to data

## **Stroop Task Trigger Information**

- 255: Presentation onset trigger
- 1+2: Recording onset (Intro screen)
- 16: Fixation Onset
- 64: Audio encoding (previous trial)
- 32: Word presentation/recording onset
- 6+8: Recording offset (i.e. 5 s following run offset)

Fix Onset (16) (1s) Stroop Start (32) (2s) GetAudio (64)
(i.e. fixation, visual word presentation onset, audio encoding)

Segmentation was done -1.5s prior to fix onset until 3s + 1.5s after fix onset.

---

# **DataLad datasets and how to use them**

This repository is a [DataLad](https://www.datalad.org/) dataset. It provides
fine-grained data access down to the level of individual files, and allows for
tracking future updates. In order to use this repository for data retrieval,
[DataLad](https://www.datalad.org/) is required. It is a free and
open source command line tool, available for all major operating
systems, and builds up on Git and [git-annex](https://git-annex.branchable.com/)
to allow sharing, synchronizing, and version controlling collections of
large files. You can find information on how to install DataLad at
[handbook.datalad.org/en/latest/intro/installation.html](http://handbook.datalad.org/en/latest/intro/installation.html).

### Get the dataset

A DataLad dataset can be `cloned` by running

```
datalad clone <url>
```

Once a dataset is cloned, it is a light-weight directory on your local machine.
At this point, it contains only small metadata and information on the
identity of the files in the dataset, but not actual *content* of the
(sometimes large) data files.

### Retrieve dataset content

After cloning a dataset, you can retrieve file contents by running

```
datalad get <path/to/directory/or/file>`
```

This command will trigger a download of the files, directories, or
subdatasets you have specified.

DataLad datasets can contain other datasets, so called *subdatasets*.
If you clone the top-level dataset, subdatasets do not yet contain
metadata and information on the identity of files, but appear to be
empty directories. In order to retrieve file availability metadata in
subdatasets, run

```
datalad get -n <path/to/subdataset>
```

Afterwards, you can browse the retrieved metadata to find out about
subdataset contents, and retrieve individual files with `datalad get`.
If you use `datalad get <path/to/subdataset>`, all contents of the
subdataset will be downloaded at once.

### Stay up-to-date

DataLad datasets can be updated. The command `datalad update` will
*fetch* updates and store them on a different branch (by default
`remotes/origin/master`). Running

```
datalad update --merge
```

will *pull* available updates and integrate them in one go.

### Find out what has been done

DataLad datasets contain their history in the ``git log``.
By running ``git log`` (or a tool that displays Git history) in the dataset or on
specific files, you can find out what has been done to the dataset or to individual files
by whom, and when.

### More information

More information on DataLad and how to use it can be found in the DataLad Handbook at
[handbook.datalad.org](http://handbook.datalad.org/en/latest/index.html). The chapter
"DataLad datasets" can help you to familiarize yourself with the concept of a dataset.
